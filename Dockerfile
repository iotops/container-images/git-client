FROM alpine
RUN apk update ;\
    apk add git openssh-client
ADD cloner.sh /usr/local/bin/cloner.sh
RUN chmod a+x /usr/local/bin/cloner.sh
ENTRYPOINT /usr/local/bin/cloner.sh
